#ifndef __MESAMONGOOSE__JAVASCRIPT__
#define __MESAMONGOOSE__JAVASCRIPT__

#include "mongoose.h"

#define SERVER "192.168.1.1"
#define PORT 502

char *mesaDados(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData);

char *executaParar(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData);

char *executaPararGravacao(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData);

char *executaEmergencia(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData);

char *executaReset(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData);

char *executaPosicionamento(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData);

char *executaJog(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData);

char *executaJogAutomatizada(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData);

char *executaHabJog(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData);

char *executaDesJog();

char *getConfig(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData);

char *setConfig(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData);

char *executaPosicaoAziScript(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData);

char *executaVelocidadeAziScript(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData);

char *executaPosicaoTiltScript(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData);

char *executaVelocidadeTiltScript(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData);

char *executaTempoScript(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData);

char *executaLimpaScript(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData);

char *executaScript(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData);

char *setAccAzi(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData);

char *setVelPosAzi(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData);

char *setAccTilt(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData);

char *setVelPosTilt(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData);

void executaPosicionamentoAzimute(float);

void executaVelocidadeAzimute(float);

void executaPosicionamenteTilt(float);

void executaVelocidadeTilt(float);

char *executaGravacao(struct mg_connection *conn,
		const struct mg_request_info *request, const char *data, int tamData);

double leInputRegs(int);

double * leDados();

#endif
