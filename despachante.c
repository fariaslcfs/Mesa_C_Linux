#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "mesamongoose.h"
#include "despachante.h"
#include "javascript.h"
#include "util.h"
#include "mesa.h"

/**
 *\file despachante.c
 * \brief arquivo responsavel tratar requisicoes
 * Arquivo responsavel por tratar requisicoes vendas do usuario
 * e despachar para seu rsespectivos
 */

static const int tamArrayDespachante = 24;

static struct entradaDespachante {
	char prefix[1024];
	char *(*fn)(struct mg_connection *conn, const struct mg_request_info *request, const char *data, int tamData);
} arrayDespachante[] = {
		"/api/dados.json", mesaDados,
	    "/api/executaParar.json", executaParar,
		"/api/executaPararGravacao.json", executaPararGravacao,
		"/api/executaReset.json", executaReset,
		"/api/emergencia.json", executaEmergencia,
		"/api/executaPosicionamento.json", executaPosicionamento,
		"/api/executaJog.json", executaJog,
		"/api/executaJogAutomatizada.json",	executaJogAutomatizada,
		"/api/executaHabJog.json", executaHabJog,
		"/api/executaDesJog.json", executaDesJog,
		"/api/setConfig.json", setConfig,
		"/api/getConfig.json", getConfig,
		"/api/executaPosicaoAziScript.json", executaPosicaoAziScript,
		"/api/executaVelocidadeAziScript.json", executaVelocidadeAziScript,
		"/api/executaPosicaoTiltScript.json", executaPosicaoTiltScript,
		"/api/executaVelocidadeTiltScript.json", executaVelocidadeTiltScript,
		"/api/executaTempoScript.json", executaTempoScript,
		"/api/executaLimpaScript.json", executaLimpaScript,
		"/api/executaScript.json", executaScript,
		"/api/setAccAzi.json", setAccAzi,
		"/api/setVelPosAzi.json", setVelPosAzi,
		"/api/setAccTilt.json", setAccTilt,
		"/api/setVelPosTilt.json", setVelPosTilt,
		"/api/executaGravacao.json", executaGravacao
};

/**@fn trataRequisicoes( stsruct mg_connection *conn, const struct mg_request_inf *request)
 \brief funcao para tratar requicoes
 Funcao responsavel por tratar requisicoes vindas do usuario e encaminhar para
 suas respectivas funcoes
 @param conn conexao do usuario
 @param request requisicao do usuario
 @return fn,Null
 */

static void *trataRequisicoes(struct mg_connection *conn, const struct mg_request_info *request) {
	char bufParametros[TAMBUFFERPARAMETROS];
	int i, tamData;
	struct entradaDespachante aux;

	for (i = 0; i < tamArrayDespachante; i++) {
		aux = arrayDespachante[i];
		if (strcmp(aux.prefix, request->uri) == 0) {
			getBufferParametros(conn, request, bufParametros, sizeof(bufParametros));
			tamData = strlen(bufParametros) + 1;
			return aux.fn(conn, request, bufParametros, tamData);
		}
	}
	return NULL;
}

/**fn eventHandler(enum mg_event, struct mg_connection *conn, const struct mg_rquest_inf *request)
 \brief funcao responsavel por pegar eventos do usuario
 Funcao respomnsavel por pegar eventos do usuario a conexao e requisicao
 esta sendo usado somente para pegar evento de nova requisicao
 @param event evento passado
 @param conn conexao
 @param request requisicao feita
 @return processed, NULL
 */

void *eventHandler(enum mg_event event, struct mg_connection *conn, const struct mg_request_info *request) {
	void *processed = (void *) "yes";

	if (event == MG_NEW_REQUEST) {
		processed = trataRequisicoes(conn, request);
	} else {
		processed = NULL;
	}
	return processed;
}

