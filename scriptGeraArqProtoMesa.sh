#!/bin/bash

# ProgressBar
sp="/-\|"
sc=0
spin() {
   printf "\b${sp:sc++:1}"
   ((sc==${#sp})) && sc=0
}
endspin() {
   printf "\r%s\n" "$@"
}

# Input variables
echo -e "Enter the initial value in decimal format with 8 decimal places at maximum."
read start
echo -e "Enter the incremental value in decimal format with 8 decimal places at maximum."
read inc
echo -e "Enter the final value in decimal format with 8 decimal places at maximum."
read end
echo -e "Enter the time in seconds between commands"
read t

# Progress bar
sp="/-\|"
sc=0
spin() {
   printf "\b${sp:sc++:1}"
   ((sc==${#sp})) && sc=0
}
endspin() {
   printf "\r%s\n" "$@"
}

# In order to handle float point number operations
nlines=$((echo scale=8 ; echo \($end / $inc + 1\)*4) | bc)

echo -e "Aguarde. Gerando arquivo protoMesa.txt contendo "${nlines%.*}" linhas" # It can used the [calc] command, but not native.
[ -e protoMesa.txt ] && rm -rf protoMesa.txt || touch protoMesa.txt
[ -e protoMesatmp.txt ] && rm -rf protoMesatmp.txt || touch protoMesatmp.txt

# loop start
for i in $(seq $start $inc $end); do echo -e 'a'$i'\ne'$t'\nc'$i'\ne'$t >> protoMesatmp.txt && spin; done
endspin
# loop end

# In order to replace comma by dot
sed s/,/./g protoMesatmp.txt >> protoMesa.txt

# Remove temp file
rm -f protoMesatmp.txt

# Copy file to Downloads folder
cp protoMesa.txt ~/Downloads/protoMesa.txt

# Last test to check file existence
[ -e protoMesa.txt ] && echo -e "Arquivo protoMesa.txt criado com sucesso" || echo -e "Falha na criação do arquivo"
