#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <modbus.h>
#include <unistd.h>
#include <signal.h>
#include <math.h>

#include "mesa.h"
#include "util.h"

#define SERVER_ID       17
#define ADDRESS_START    0
#define ADDRESS_END     99

int main(int argc, char **argv)
{
    modbus_t *ctx;
    uint16_t dado;
    int i;

    if(argc == 2){
       i = atoi(argv[1]);
    } else {
       fprintf(stderr, "usage: lereg <endereco>\n");
       exit(0);
    }


    ctx = modbus_new_tcp(SERVER, PORT);
    ///modbus_set_debug(ctx, TRUE);

    if (modbus_connect(ctx) == -1) {
        fprintf(stderr, "Connection failed: %s\n",
                modbus_strerror(errno));
        modbus_free(ctx);
        return -1;
    }
 
    modbus_read_registers(ctx, i, 1, &dado);
    fprintf(stdout, "Dado lido[%d]: %X ou %d\n", i, dado, dado); 
    /* Close the connection */
    modbus_close(ctx);
    modbus_free(ctx);

    return 0;
}
