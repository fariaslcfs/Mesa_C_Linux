#!/usr/bin/perl
use strict;
use warnings;
use CGI qw( :standard );
use CGI::Carp qw( fatalsToBrowser );


my $query = CGI->new();

#print $query->header("text/html");

my $time       = $query->param('inctime');
my $incveltilt = $query->param('incveltilt');
my $incvelazi  = $query->param('incvelazi');
my $veltiltini = $query->param('velinitilt');
my $veltiltend = $query->param('velendtilt');
my $velaziini  = $query->param('veliniazi');
my $velaziend  = $query->param('velendazi');

if ( $veltiltend > 200 ) {
	$veltiltend = 200;
}

if ( $veltiltend < -200 ) {
	$veltiltend = -200;
}

if ( $velaziend > 500 ) {
	$velaziend = 500;
}

if ( $velaziend < -500 ) {
	$velaziend = -500;
}

my $accveltilt    = $veltiltini;
my $accvelazi     = $velaziini;
my $limittiltloop = abs($veltiltend);
my $limitaziloop  = abs($velaziend);


while ( (abs($accveltilt) < $limittiltloop) or (abs($accvelazi) < $limitaziloop) ){
	$accveltilt = $accveltilt + $incveltilt;
	if (abs($accveltilt) >= $limittiltloop ){
		$incveltilt = 0;
		$accveltilt = $veltiltend;
	}

	$accvelazi = $accvelazi + $incvelazi;
	if (abs($accvelazi) >= $limitaziloop){
		$incvelazi = 0;
		$accvelazi = $velaziend;
	}

	#print $accveltilt, "  ", $accvelazi, "\n";

	system("perl velocidade_normal.pl $accvelazi $accveltilt");

	sleep $time;
}

system("perl velocidade_normal.pl 0 0");
sleep 2;
system("perl posicionamento_normal.pl 0 0");

print redirect(-location=>'http://127.0.0.1:8081/velautomatiza.html');

exit;




