#!/usr/bin/perl

use strict;
use CGI;

# print "usage: <program> <velAzi> <velTilt>\n";


`tools/escrevereg 1 22`;        # 0 means tilt in jog mode - 1 means in normal mode
`tools/escrevereg 10000 40`;    #aceleracao
`tools/escrevereg 10000 42`;    #desaceleracao

`tools/escrevereg 1 24`;        # 0 means azimute in jog mode - 1 means in normal mode
`tools/escrevereg 10000 50`;    #aceleracao
`tools/escrevereg 10000 52`;    #desaceleracao

my $vAzi   = $ARGV[0] * 100;
if (abs($vAzi) > 50000){
	if($vAzi < 0){
		$vAzi = -50000;
	}
	else{
		$vAzi = 50000;	
	}
}

my $vTilt   = $ARGV[1] * 100;
if (abs($vTilt) > 20000){
	if($vTilt < 0){
		$vTilt = -20000;
	}
	else{
		$vTilt = 20000;
	}
}


if ( $vTilt < 0 ) {
    $vTilt = abs($vTilt);
	`tools/escrevereg $vTilt 46`;
	`tools/escrevereg 2 26`;      #reverso
}
else{
	`tools/escrevereg $vTilt 46`;
	`tools/escrevereg 1 26`;      #avante
}

if($vAzi < 0){
	$vAzi = abs($vAzi);
	`tools/escrevereg $vAzi 56`;
	`tools/escrevereg 2 28`;      #reverso
}	
else {
	`tools/escrevereg $vAzi 56`;
	`tools/escrevereg 1 28`;      #avante
}

1;
