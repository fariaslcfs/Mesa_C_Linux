#!/usr/bin/perl

use strict;

#print "usage: <program> <posAzi> <posTilt>\n";

# Os valores de azimute e tilt devem ser enviados com a função escreveregs
# porque os valores são passados em dois registradores e a função escreveregs
# não está disponível no código fonte velho antes da instalação
# Solução --> implementar a função escreveregs
# Como os registradores são de 16 bits, deve-se usar dois registradores caso o 
# valor seja maior que 65535 (64kB)
# Tilt 60 e 61 
# Azimute 70 e 71
# Feito!

# Falta ajustar no escreveregs.c o sinal do valor e o sentido de rotação ...
# Feito!


my $posAzi = $ARGV[0];
if ($posAzi < 0){
	$posAzi = 360 - abs($posAzi);
}
my $pAzi = $posAzi * 1000;

my $posTilt = $ARGV[1];
if ($posTilt < 0){
	$posTilt = 360 - abs($posTilt);
}
my $pTilt = $posTilt * 1000;

# Tilt
`tools/escrevereg 2 22`; 
`tools/escrevereg 0 26`;
`tools/escreveregs $pTilt 60`;
`tools/escrevereg  10000 64`;  	#aceleracao (Decimo)
`tools/escrevereg  10000 66`;  	#desaceleracao (Decimo)
`tools/escrevereg  3000 62`;  	#Velocidade (Decimo)
sleep 1;
`tools/escrevereg 0 26`; 
sleep 1;
`tools/escrevereg 4 26`;

# Azimuth
`tools/escrevereg 2 24`;
`tools/escreveregs $pAzi 70`;
`tools/escrevereg  10000 50`;  	#aceleracao (Decimo)
`tools/escrevereg  10000 52`;  	#desaceleracao (Decimo)
`tools/escrevereg  3000 56`;  	#Velocidade (Decimo)
sleep 1;
`tools/escrevereg 0 28`;
sleep 1;
`tools/escrevereg 4 28`;

exit;
